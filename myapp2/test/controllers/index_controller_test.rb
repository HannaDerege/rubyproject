require 'test_helper'

class IndexControllerTest < ActionDispatch::IntegrationTest
  test "should get reporter" do
    get index_reporter_url
    assert_response :success
  end

end
