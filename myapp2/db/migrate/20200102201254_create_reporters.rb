class CreateReporters < ActiveRecord::Migration[6.0]
  def change
    create_table :reporters do |t|
      t.string :name
      t.string :blood_type
      t.string :case
      t.string :gender

      t.timestamps
    end
  end
end
