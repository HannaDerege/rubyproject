class CaseController < ApplicationController
  def index
    @cases = Reporter.all
  end

 def create
     @case = Reporter.new(case_params)
     @case.save
     redirect_to @case
  end 

  def show
     @case = Reporter.find(params[:id])
  end
  
  def edit
     @case = Reporter.find(params[:id])
  end

  def update 
     @case = Reporter.find(params[:id])
     if @case.update(case_params)
        redirect_to @case
     else
        render 'edit'
     end
  end
  def destroy
     @case = Reporter.find(params[:id])
     @case.destroy
  end
	
  private 
   def case_params
      params.require(:case).permit(:name, :blood_type, :case, :gender)
   end
 
end
